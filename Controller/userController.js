const pool = require('../config/database')

async function getUsers(req, res) {
    const user = await pool.query('SELECT * FROM public.pengguna')
    console.log(user);
    const usersData = user.rows;
    res.status(200).json({
        "test": "Menampilkan semua user",
        "data": usersData
    })
}

async function CreateUsers(req, res) {
    const{id, name} = req.body;
    const user = await pool.query(`INSERT INTO public.pengguna (id, name) VALUES ('${id}', '${name}')`)
    console.log(user);
    const usersData = user.rows;

    res.status(201).json({
        "test": "Berhasil menambahkan user",
    })
}

module.exports = {
    getUsers,
    CreateUsers
};