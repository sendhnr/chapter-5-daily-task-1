const router = require('express').Router();

const userController = require('../Controller/userController');
const products = require('../Controller/products');

//membuat routing API
router.get('/api/user', userController.getUsers);
router.post('/api/user', userController.CreateUsers);

module.exports = router;