const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const PORT = 8000

const routes = require('./routes');

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

app.use(routes);

app.listen(PORT, () => {
    console.log(`server is listening on http://localhost:${PORT}`);
});